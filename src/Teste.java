import br.com.cesmac.genalg.app.AlgoritmoGenetico;
import br.com.cesmac.genalg.domain.Individuo;


public class Teste {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AlgoritmoGenetico ag = new AlgoritmoGenetico(100,1000);
		//System.out.println(ag.getPopulacao().toString());
		
		Individuo melhorIndividuo = ag.run();
		
		System.out.println(melhorIndividuo.toString());

	}

}
