package br.com.cesmac.genalg.app;

import br.com.cesmac.genalg.domain.Individuo;
import br.com.cesmac.genalg.domain.Par;
import br.com.cesmac.genalg.domain.Populacao;
import br.com.cesmac.genalg.model.CaixeiroModel;

public class AlgoritmoGenetico {
	/**
	 * O tamanho da população inicial.
	 */
	private int tamanhoPopulacaoInicial;
	
	/**
	 * O número de iterações
	 */
	private int numeroIteracoes;
	
	private CaixeiroModel caixeiro = CaixeiroModel.getInstance();
	
	/**
	 * A População inicial
	 */
	private Populacao populacao = new Populacao();
	
	public AlgoritmoGenetico(int tamanhoPopulacaoInicial, int numeroIteracoes) {
		this.tamanhoPopulacaoInicial = tamanhoPopulacaoInicial;
		this.numeroIteracoes = numeroIteracoes;
	}
	
	public Populacao getPopulacao(){
		return this.populacao;
	}
	
	public Individuo run(){
		int t = 0;
		Populacao novaPopulacao = new Populacao();
		novaPopulacao.setGeracao(populacao.getGeracao() + 1);
		
		//gerar população aleatória:
		populacao.initialPopulation(tamanhoPopulacaoInicial);
		double melhorFitnessAtual = populacao.getMelhorIndividuo().getValorFitness();
		
		while (t <= numeroIteracoes){
		//while(melhorFitnessAtual > this.caixeiro.getLimiar()){
			
			for (int i = 0; i < populacao.getTamamho(); i++) {
				// 1 - seleção.
				Par par = populacao.selecionar();
				
				// 2 - reprodução.
				Individuo filho = par.reproduzir();
				
				// 3 - mutação.
				filho.mutar();
				
				novaPopulacao.add(filho);
			}
			populacao = novaPopulacao;
			novaPopulacao = new Populacao();
			novaPopulacao.setGeracao(populacao.getGeracao() + 1);
			
			t++;
			System.out.println("Geração: " + populacao.getGeracao());
		}
		
		
		
		return populacao.getMelhorIndividuo();
	}

}
