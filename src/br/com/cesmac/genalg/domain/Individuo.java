package br.com.cesmac.genalg.domain;

import java.util.Random;

import br.com.cesmac.genalg.model.CaixeiroModel;

public class Individuo {
	
	/**
	 * Obtém o valor da função fitness para esse indivíduo, que aqui
	 * corresponde à seguinte função: f(x) = Dist(1,2)+Dist(2,3)+...+Dist(n-1,n)+Dist(n,1)
	 * 
	 * @return o valor fitness para esse indivíduo.
	 */
	private int[] cromossomo;
	private CaixeiroModel caixeiro = CaixeiroModel.getInstance();
	
	public Individuo(int tam){
		this.cromossomo = new int[tam];
		for (int i = 0; i < tam; i++) {
			//escolhendo aleatoriamente um número inteiro entre 1 e 8
			int alelo = 1 + (int) (Math.random()*tam);
			while (!notInCromossomo(alelo)){
				alelo = 1 + (int) (Math.random()*tam);
			}
			this.cromossomo[i] = alelo;
		}
	}
	
	public Individuo(int[] cromossomo){
		this.cromossomo = cromossomo;
	}
	
	private boolean notInCromossomo(int alelo) {
			for(int c: this.cromossomo){
				if(c == alelo){
					return false;
				}
			}
		return true;
	}
	
	public int[] getCromossomo(){
		return this.cromossomo;
	}

	public double getValorFitness() {
		double result = 0;
		for (int i = 0; i < this.cromossomo.length; i++){
			if(i < this.cromossomo.length -1){
				result += this.caixeiro.getDistancia(this.cromossomo[i], this.cromossomo[i+1]);
			}else{
				result += this.caixeiro.getDistancia(this.cromossomo[i], this.cromossomo[0]);
			}
		}
		
		return result;
	}
	
	/**
	 * Implementação de crossover baseado em ordem:
	 * @param individuo
	 * @param pontoCrossover1
	 * @param pontoCrossover2
	 * @return
	 */
	@SuppressWarnings("null")
	public Individuo reproduzir(Individuo individuo, int pontoCrossover1, int pontoCrossover2) {
		int[] filho = new int[this.cromossomo.length];
		int[] aux = new int[this.cromossomo.length - 2];
		int i = 0;
		//1) Passa os alelos dos pontos de corte do pai1 para o filho:
		filho[pontoCrossover1] = this.cromossomo[pontoCrossover1];
		filho[pontoCrossover2] = this.cromossomo[pontoCrossover2];
		
		//2) cria uma lista dos alelos restantes do pai1:
		for(int a = 0; a < this.cromossomo.length; a++){
			if(a != pontoCrossover1 && a != pontoCrossover2){
				aux[i] = this.cromossomo[a];
				i++;
			}
		}
		
		//3) ordena esta lista de acordo com os alelos de pai2:
		aux = individuo.ordenaRelativo(aux);
		
		//4) preenche o restante do filho com a sequencia de aux:
		i = 0;
		for (int a = 0;  a < this.cromossomo.length; a++){
			if (a != pontoCrossover1 && a != pontoCrossover2){
				filho[a] = aux[i];
				i++;
			}
		}
		
		return new Individuo(filho);
	}
	
	private int[] ordenaRelativo(int[] sequencia){
		int aux = 0;
		for(int i = 0; i < sequencia.length; i++){
			for(int j = i+1; j < sequencia.length;j++){
				if(getIndice(sequencia[i]) > getIndice(sequencia[j])){
					aux = sequencia[i];
					sequencia[i] = sequencia[j];
					sequencia[j] = aux;
				}
			}
		}
		return sequencia;
	}

	private int getIndice(int valor) {
		for(int i = 0; i < this.cromossomo.length; i++){
			if (this.cromossomo[i] == valor){
				return i;
			}
		}
		return -1;
	}
	public void mutar() {
		Random random = new Random();
		// Gerando um valor de zero a cem.
		int testeMutacao = random.nextInt(101);
		
		// Se o valor gerado ficar abaixo da probabilidade de mutação...
		if (testeMutacao < this.caixeiro.getProbabilidadeMutacao()) {
			// O corte deve ser feito em qualquer posição de 0..7
			int posicaoCadeia = random.nextInt(this.caixeiro.getNumCidades() - 1);
			// O valor inserido será qualquer cidade (1..8)
			int numeroMutacao = random.nextInt(this.caixeiro.getNumCidades()) + 1;
			
			//fazer troca do valor:
			this.cromossomo[this.getIndice(numeroMutacao)] = this.cromossomo[posicaoCadeia];
			this.cromossomo[posicaoCadeia] = numeroMutacao;
		}
	}
	
	public String toString(){
		StringBuilder result = new StringBuilder();
		result.append("cromossomo([");
		
		for (int i = 0; i < cromossomo.length; i++) {
			result.append(cromossomo[i]);
			
			if (i < cromossomo.length - 1) {
				result.append(", ");
			}
		}
		
		result.append("]) = ").append(getValorFitness());
		//result.append("])");
		
		return result.toString();
	}
		

}
