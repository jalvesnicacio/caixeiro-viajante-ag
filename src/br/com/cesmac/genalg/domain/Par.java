package br.com.cesmac.genalg.domain;

import java.util.Random;

public class Par {

	/**
	 * O indivíduo 1 do par.
	 */
	private Individuo individuo1;
	
	/**
	 * O indivíduo 2 do par.
	 */
	private Individuo individuo2;
	
	/**
	 * Constrói um novo par.
	 * 
	 * @param individuo1 o indivíduo 1.
	 * @param individuo2 o indivíduo 2.
	 */
	public Par(Individuo individuo1, Individuo individuo2) {
		this.individuo1 = individuo1;
		this.individuo2 = individuo2;
	}

	public Individuo getIndividuo1() {
		return individuo1;
	}

	public Individuo getIndividuo2() {
		return individuo2;
	}
	
	/**
	 * Faz o par se reproduzir, gerando um novo indivíduo.
	 * 
	 * @return o novo indivíduo gerado.
	 */
	public Individuo reproduzir() {
		// Sorteando o ponto de crossover.
		Random random = new Random();
		int pontoCrossover1 = random.nextInt(individuo1.getCromossomo().length-1);
		int pontoCrossover2 = random.nextInt(individuo1.getCromossomo().length-1);
		while (pontoCrossover1 == pontoCrossover2){
			pontoCrossover2 = random.nextInt(individuo1.getCromossomo().length-1);
		}
		return individuo1.reproduzir(individuo2, pontoCrossover1, pontoCrossover2);
	}


}
