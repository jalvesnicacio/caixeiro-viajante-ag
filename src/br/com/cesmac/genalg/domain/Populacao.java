package br.com.cesmac.genalg.domain;

import java.util.ArrayList;
import java.util.List;

import br.com.cesmac.genalg.model.CaixeiroModel;

public class Populacao {
	private List<Individuo> individuos;
	private CaixeiroModel caixeiro;
	private int geracao;
	
	public Populacao() {
		this.individuos = new ArrayList<Individuo>();
		this.caixeiro = CaixeiroModel.getInstance();
	}
	
	public void initialPopulation(int num){
		for(int i = 0; i <= num; i++){
			this.individuos.add(new Individuo(this.caixeiro.getNumCidades()));
		}
		this.geracao = 0;
	}

	public Par selecionar() {
		// Somar os valores de fitness de todos os indivíduos.
		double totalFitness = 0;
		
		for (Individuo i: individuos) {
			totalFitness += i.getValorFitness();
		}
		
		// Gerar um par.
		Individuo individuo1 = rodarRoleta(totalFitness);
		Individuo individuo2 = rodarRoleta(totalFitness);
		
		return new Par(individuo1, individuo2);
	}

	private Individuo rodarRoleta(double valorMaximoSorteio) {
		double random = Math.random() * valorMaximoSorteio;
		double soma = 0;
		
		// Passo 3: Rodar a roleta.
		for (Individuo i: individuos) {
			soma += i.getValorFitness();
			
			if (soma > random) {
				return i;
			}
		}
		
		// Este algoritmo nunca retornará null...
		return null;
	}

	public int getTamamho() {
		return this.individuos.size();
	}

	public void add(Individuo filho) {
		this.individuos.add(filho);
	}

	public Individuo getMelhorIndividuo() {
		Individuo result = this.individuos.get(0);
		double melhorFitness = result.getValorFitness();
		
		for (Individuo i: individuos) {
			double valorFitness = i.getValorFitness();
			
			if (valorFitness < melhorFitness) {
				melhorFitness = valorFitness;
				result = i;
			}
		}
		
		return result;
	}
	
	
	
	public int getGeracao() {
		return geracao;
	}

	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}

	/**
	 * Retorna uma representação dessa população em String.
	 */
	public String toString() {
		StringBuilder result = new StringBuilder();
		
		for (Individuo i: individuos) {
			result.append(i.toString()).append("\n");
		}
		
		return result.toString();
	}

}
