package br.com.cesmac.genalg.model;


public class CaixeiroModel {
	
	/**
	 * Porcentagem de mutação. Exemplo: valor 10 => 10%.
	 */
	private static final int PROBABILIDADE_MUTACAO = 90;
	
	private static final double LIMIAR = 100;
	
	private int numCidades;
	private int distancia[][] = new int[numCidades][numCidades];
	
	private static CaixeiroModel caixeiroInstance;
	
	private CaixeiroModel(){
		int d[][] = {
				{0,11,20,27,40,43,39,28},
				{11,0,9,16,29,32,28,19},
				{20,9,0,7,20,22,19,15},
				{27,16,7,0,13,16,12,13},
				{40,29,20,13,0,3,2,21},
				{43,32,22,16,3,0,4,23},
				{39,28,19,12,2,4,0,22},
				{28,19,15,13,21,23,22,0}
		};
		this.distancia = d;
		this.numCidades = 8;
	}
	
	public static CaixeiroModel getInstance(){
		if(caixeiroInstance == null){
			caixeiroInstance = new CaixeiroModel();
		}
		return caixeiroInstance;
	}
	
	public int getNumCidades(){
		return this.numCidades;
	}
	
	public int getProbabilidadeMutacao(){
		return CaixeiroModel.PROBABILIDADE_MUTACAO;
	}
	
	public double getLimiar(){
		return CaixeiroModel.LIMIAR;
	}
	
	/**
	 * Calcula a distancia entre duas estaçoes
	 */
	public int getDistancia(int origem, int destino){
		origem = origem - 1;
		destino = destino - 1;
		return this.distancia[origem][destino];
	}
	
	
	
}
